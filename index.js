window.onload = async function () {
    const params = {
        latitude: 24.482103,
        longitude: 39.556877,
        current: "temperature_2m,weather_code",
        daily: "temperature_2m_max,temperature_2m_min",
        timezone: "Europe/Moscow",
        forecast_days: 1
    };

    const url = new URL("https://api.open-meteo.com/v1/forecast");
    Object.entries(params).forEach(([key, value]) => url.searchParams.append(key, value));

    try {
        const { daily, current } = await (await fetch(url)).json();

        ['maxTemp', 'minTemp', 'currentTemp'].forEach((id, i) => {
            let temp = [daily.temperature_2m_max[0], daily.temperature_2m_min[0], current.temperature_2m][i];
            document.getElementById(id).textContent = Math.round(temp);
        });              

        const weatherCodes = {
            0: 'fair',
            1: 'mainly clear',
            2: 'partly cloudy',
            3: 'overcast',
            51: 'light drizzle',
        };

        document.getElementById('weather').textContent = weatherCodes[current.weather_code];
    } catch (error) {
        console.error('Error:', error);
    }
}